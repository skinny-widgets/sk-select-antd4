
import { AntdSkSelect }  from '../../sk-select-antd/src/antd-sk-select.js';

export class Antd4SkSelect extends AntdSkSelect {

    get prefix() {
        return 'antd4';
    }

}
